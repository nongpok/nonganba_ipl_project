const fs = require('fs');

function generateJsonFiles(inputData, outputFile){

    fs.writeFile(outputFile, JSON.stringify(inputData), "utf-8", function(err) {
        if(err){
            console.log(err);
        }else{
            console.log(`Output file saved in ${outputFile}`);
        }
    });
}

module.exports = generateJsonFiles;